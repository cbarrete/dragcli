# Usage

`dragcli` selects the files passed as arguments (e.g. `dragcli file1 file2`).
Click anywhere to drop them.

`dragcli` reports non-existing files to `stderr`.

# Building

`dragcli` depends on Qt (`qtbase5-dev` on Debian-based systems for the
compile-time dependencies). Build it with `qmake && make`.
